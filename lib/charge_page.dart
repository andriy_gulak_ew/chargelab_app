import 'package:chargelab_app/mocks/account_mock_data.dart';
import 'package:chargelab_app/mocks/charge_items_mock_data.dart';
import 'package:chargelab_app/tabbed_charges_widget.dart';
import 'package:flutter/material.dart';

enum ChargeStatus {
  DEFAULT,
  SUCCESS,
  FAILURE,
}

class ChargePage extends StatefulWidget {
  ChargePage({Key key, this.title, this.account}) : super(key: key);

  final String title;
  final AccountDataModel account;

  @override
  _ChargePageState createState() => _ChargePageState();
}

class _ChargePageState extends State<ChargePage> {
  ChargeStatus currentStatus = ChargeStatus.DEFAULT;
  final stationInputController = TextEditingController();

  // TODO: just for demo purposes
  ChargeItemDataModel chargeItem = chargeItemsData[0];

  // TODO: just for demo purposes
  void _changeStatusSuccess() {
    setState(() {
      currentStatus = ChargeStatus.SUCCESS;
    });
  }

  // TODO: just for demo purposes
  void _changeStatusDefault() {
    setState(() {
      currentStatus = ChargeStatus.DEFAULT;
    });
  }

  // TODO: just for demo purposes
  _onTextChange() {
    if (stationInputController.text == '2102') {
      _changeStatusSuccess();
    } else {
      _changeStatusDefault();
    }
  }

  @override
  void initState() {
    super.initState();
    // Start listening to changes
    stationInputController.addListener(_onTextChange);
  }

  @override
  void dispose() {
    stationInputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildInputWidget(currentStatus),
          _buildHintLabelWidget(currentStatus),
          currentStatus == ChargeStatus.SUCCESS
              ? _buildDetailsWidget()
              : Expanded(child: TabbedChargesWidget())
        ],
      ),
      drawer: _buildNavigationDrawer(context),
    );
  }

  Drawer _buildNavigationDrawer(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(widget.account.name),
            accountEmail: Text(widget.account.email),
            currentAccountPicture: CircleAvatar(
              child: Image.network(widget.account.imageSrc),
            ),
          ),
          Container(
            decoration: BoxDecoration(color: Colors.lightBlue[50]),
            child: ListTile(
              leading: Icon(Icons.ev_station),
              title: Text('Charge'),
              onTap: () {
                Navigator.of(context).pop();
              },
              selected: true,
            ),
          ),
          ListTile(
            leading: Icon(Icons.map),
            title: Text('Map'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.payment),
            title: Text('Payments'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.favorite),
            title: Text('Facorites'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.account_circle),
            title: Text('Profile'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.help),
            title: Text('Help & Feedback'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.info),
            title: Text('Terms of Service'),
            onTap: () {},
          ),
          Divider(),
          Padding(
            padding: EdgeInsets.fromLTRB(12.0, 8.0, 0, 12.0),
            child: Text(
              "Logout",
              style: TextStyle(fontSize: 14.0, color: Colors.grey),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDetailsWidget() {
    return Expanded(
      child: ListView(shrinkWrap: true, children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0),
          child: SizedBox(
            width: double.infinity,
            child: Card(
                child: Column(
              children: <Widget>[
                ClipRRect(
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(4.0)),
                  child: Image(
                    fit: BoxFit.cover,
                    image: AssetImage(chargeItem.imageSrc),
                    width: double.infinity,
                    height: 200.0,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              chargeItem.title,
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                                fontSize: 20.0,
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.fromLTRB(0.0, 4.0, 0.0, 0.0),
                              child: Text(
                                chargeItem.address,
                                style: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          child: Text("Available",
                              style: TextStyle(color: Colors.white)),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(24.0)),
                              color: Colors.lightGreen),
                          padding: EdgeInsets.fromLTRB(12.0, 4.0, 8.0, 4.0),
                        ),
                      ]),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: <Widget>[
                      Text(
                        chargeItem.details,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          "Public Level 2 charging stations. Located in the back parking lot of 54 Main Street.",
                          style: TextStyle(color: Colors.grey[500]),
                        ),
                      ),
                    ],
                  ),
                ),
                ButtonTheme(
                    minWidth: 200.0,
                    height: 40.0,
                    child: RaisedButton(
                      child: Text(
                        'Start Charge'.toUpperCase(),
                        style: TextStyle(fontSize: 16.0),
                      ),
                      color: Colors.lightGreen,
                      elevation: 4.0,
                      textColor: Colors.white,
                      splashColor: Colors.lightGreen,
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(6.0)),
                      onPressed: () {},
                    )),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "You will be billed \$1.50/hour",
                        style: TextStyle(color: Colors.grey[700]),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Report a problem",
                        style: TextStyle(color: Colors.red),
                      ),
                    ],
                  ),
                ),
              ],
            )),
          ),
        )
      ]),
    );
  }

  Widget _buildHintLabelWidget(ChargeStatus status) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30.0, 6.0, 0, 0),
      child: status == ChargeStatus.SUCCESS
          ? Text("Charging station identified",
              style: TextStyle(color: Colors.green, fontSize: 12))
          : Text("Enter a ChargeLab Station ID",
              style: TextStyle(color: Colors.grey, fontSize: 12)),
    );
  }

  Widget _buildInputWidget(ChargeStatus status) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0),
      child: TextFormField(
        controller: stationInputController,
        keyboardType: TextInputType.number,
        style: TextStyle(color: Colors.black, height: 1.3),
        decoration: InputDecoration(
            labelStyle: TextStyle(
                color: status == ChargeStatus.SUCCESS
                    ? Colors.green
                    : Colors.grey),
            labelText: "Station ID",
            filled: true,
            fillColor: Colors.grey[300],
            border: InputBorder.none,
            focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.green, width: 2.0)),
            suffixIcon: status == ChargeStatus.SUCCESS
                ? Icon(Icons.check, color: Colors.green)
                : Icon(Icons.ev_station, color: Colors.grey[900])),
      ),
    );
  }
}
