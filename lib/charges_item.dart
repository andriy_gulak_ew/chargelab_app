import 'package:chargelab_app/mocks/charge_items_mock_data.dart';
import 'package:flutter/material.dart';

class ChargeItem extends StatefulWidget {
  ChargeItem(this.chargeItem, {Key key}) : super(key: key);
  final ChargeItemDataModel chargeItem;

  @override
  _ChargeItemState createState() => _ChargeItemState(this.chargeItem);
}

class _ChargeItemState extends State<ChargeItem> {
  bool _isFavorite = false;

  _ChargeItemState(ChargeItemDataModel chargeItem);

  void handleAddToFavoriteTap() {
    setState(() {
      _isFavorite = !_isFavorite;
    });
  }

  Widget _buildLeftSection() {
    return Container(
      constraints: BoxConstraints(maxHeight: 72.0, maxWidth: 72.0),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ClipRRect(
          borderRadius: new BorderRadius.circular(8.0),
          child: Image(
            image: AssetImage(widget.chargeItem.imageSrc),
            fit: BoxFit.cover,
            width: 72.0,
            height: 72.0,
          ),
        ),
      ),
    );
  }

  Widget _buildMiddleSection() {
    return Expanded(
      child: Container(
        constraints: BoxConstraints(maxHeight: 72.0),
        padding: EdgeInsets.only(top: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Text(
              widget.chargeItem.title,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w600,
                fontSize: 16.0,
              ),
            ),
            Text(
              widget.chargeItem.address,
              style: TextStyle(color: Colors.grey[700]),
            ),
            Text(
              widget.chargeItem.details,
              style: TextStyle(color: Colors.grey[700]),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildRightSection(bool isActive) {
    return GestureDetector(
      onTap: handleAddToFavoriteTap,
      child: Container(
        constraints: BoxConstraints(maxHeight: 72.0),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0.0, 8.0, 12.0, 0.0),
          child: Icon(
            Icons.favorite,
            color: isActive ? Colors.red : Colors.grey,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        _buildLeftSection(),
        _buildMiddleSection(),
        _buildRightSection(_isFavorite),
      ],
    );
  }
}
