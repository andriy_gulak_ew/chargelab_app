class ChargeItemDataModel {
  String title;
  String address;
  String details;
  String imageSrc;

  ChargeItemDataModel(this.title, this.address, this.details, this.imageSrc);
}

List<ChargeItemDataModel> chargeItemsData = [
  ChargeItemDataModel("Public Charger", "Statuin 1372, 54 Main Street",
      "Level 2 • 7.2 kW • \$1.50/hour", 'assets/1.png'),
  ChargeItemDataModel("Collab Space", "Statuin 2102, 70 Bongard Ave",
      "Level 2 • 7.2 kW • Free for Employees", 'assets/2.png'),
  ChargeItemDataModel("Tim Hortons", "Statuin 7138, 10228 Highway 11",
      "Level 3 • 48 kW • \$15/hour", 'assets/3.png'),
];
