class AccountDataModel {
  String name;
  String email;
  String imageSrc;

  AccountDataModel(this.name, this.email, this.imageSrc);
}

var accountData = AccountDataModel("Jane Li", "jane.li@gmail.com",
    "https://www.shareicon.net/data/128x128/2016/05/26/771202_people_512x512.png");
