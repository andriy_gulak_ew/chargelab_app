import 'package:chargelab_app/charges_item.dart';
import 'package:chargelab_app/mocks/charge_items_mock_data.dart';
import 'package:flutter/material.dart';

class TabbedChargesWidget extends StatelessWidget {
  final numberOfTabs = 2;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: numberOfTabs,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          bottom: TabBar(
            tabs: [
              Tab(
                  child: Text('recent'.toUpperCase(),
                      style: TextStyle(color: Colors.grey[900]))),
              Tab(
                  child: Text('favorites'.toUpperCase(),
                      style: TextStyle(color: Colors.grey[900]))),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            ListView.separated(
              itemCount: chargeItemsData.length,
              itemBuilder: (context, index) {
                return ChargeItem(chargeItemsData[index]);
              },
              separatorBuilder: (context, index) {
                return Divider(
                  indent: 60.0,
                );
              },
            ),
            Center(child: Text("Nothing here yet"))
          ],
        ),
      ),
    );
  }
}
