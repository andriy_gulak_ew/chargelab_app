import 'package:chargelab_app/charge_page.dart';
import 'package:chargelab_app/mocks/account_mock_data.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'ChargeLab App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ChargePage(title: 'Charge', account: accountData),
    );
  }
}
